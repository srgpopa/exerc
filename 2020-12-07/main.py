# """"example-02.py reimplements FileOpen from example-01.py as a
# function-based context manager"""
# import contextlib
#
#
# @contextlib.contextmanager
# def file_open(filename, mode):
#     print("Opening file")
#     f = open(filename, mode)
#     print("Starting with... block")
#     yield f  # similar to a <partial> return
#     print("Closing file")
#     f.close()
#
#
# if __name__ == "__main__":
#     path = "main.py"
#     with file_open(path, "r") as f:
#         print(f"{path} has {len(f.readlines())} lines")
#
#     print("Finished")
#
# a = ""
# b = None
# print(type(a))
# print(type(b))
#
# """
# Write a `timeit` context manager which saves a current `time.time()`
# value on entry and prints time passed since entry on exit. Try
# splitting the following chunk of text a million times inside the
# context to test it. Use punctuation mark as a delimiter.
#     Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
#     eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
#     enim ad minim veniam, quis nostrud exercitation ullamco laboris
#     nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
#     in reprehenderit in voluptate velit esse cillum dolore eu
#     fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
#     proident, sunt in culpa qui officia deserunt mollit anim id est
#     laborum.
# """
# import time
# class Timeit:
#     def __init__(self):
#         self.text = """Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
#     eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
#     enim ad minim veniam, quis nostrud exercitation ullamco laboris
#     nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
#     in reprehenderit in voluptate velit esse cillum dolore eu
#     fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
#     proident, sunt in culpa qui officia deserunt mollit anim id est
#     laborum."""
#         self.timp_curent = 0
#     def __enter__(self):
#         self.timp_curent = time.time()
#         print(self.timp_curent)
#         return self.text
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         diferenta = time.time() - self.timp_curent
#         print(time.time())
#         print(diferenta)
# if __name__ == "__main__":
#     with Timeit() as f:
#         for i in range(0, 1000000):
#             f.split(".")
#
# import contextlib
# import time
# @contextlib.contextmanager
# def timeit(text):
#     timp_curent = time.time()
#     print(timp_curent)
#     yield text
#     diferenta = time.time() - timp_curent
#     print(time.time())
#     print(diferenta)
# if __name__ == "__main__":
#     text_final = """Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
#     eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
#     enim ad minim veniam, quis nostrud exercitation ullamco laboris
#     nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
#     in reprehenderit in voluptate velit esse cillum dolore eu
#     fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
#     proident, sunt in culpa qui officia deserunt mollit anim id est
#     laborum."""
#     with timeit(text_final) as f:
#         for i in range(0, 1000000):
#             f.split()
#
# fruit = ['apples', 'bananas', 'oranges']
# capitalized_fruit = list(map(lambda s: s.capitalized(), fruit))

# print(capitalized_fruit)

# from pprint import pprint


# class Plant:
#     def __init__(self, specie):
#         self.specie = specie

#     def __str__(self):
#         return f'These are {self.specie}'

#     def __repr__(self):
#         return self.__str__()


# fruit = ['apples', 'bananas', 'oranges']
# capitalized_fruit = list(map(lambda s: Plant(s), fruit))

# pprint(capitalized_fruit)
# print(capitalized_fruit)
#
# import datetime
# class User:
#     # registered_on = datetime.time() 
#     # number_of_loggins = 0
#     # last_seen = datetime.time()
# 
#     def __init__(self, registered_on = datetime.time(), number_of_loggins = 0, last_seen = datetime.time()):
#         self.registered_on = registered_on
#         self.number_of_loggins = number_of_loggins
#         self.last_seen = last_seen
# 
#     def __repr__(self):
#         return f'User registered on {self.registered_on}, logged in a total number of {self.number_of_loggins} and last login was on {self.last_seen}\n'
# 
# 
# user1 = User(number_of_loggins = 23) 
#     
# user2 = User(number_of_loggins = 5)
# 
# ljst_of_users = [user1, user2]
# 
# print(ljst_of_users)
# 
# ljst_of_users.sort(key = lambda s: s.number_of_loggins, reverse = True)
# 
# print(ljst_of_users)